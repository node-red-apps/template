FROM nodered/node-red:1.2.7-minimal

# Clean up the data directory to make sure we
# don't grab files from the base Node-RED container.
RUN rm -f /data/* /data/.[a-z]*

# The default is to disable the IDE (flow editor)
# for our standalone container.
ENV NODE_RED_ADMIN_ROOT=false

COPY *.js *.json /data/
RUN cd /data && \
    FILE=$(grep flowFile package.json | cut -f4 -d\") && \
    ([ "${FILE}" != "flows.json" ] && mv "${FILE}" flows.json) ; \
    FILE=$(grep credentialsFile package.json | cut -f4 -d\") && \
    ([ "${FILE}" != "flows_cred.json" ] && mv "${FILE}" flows_cred.json) ; \
    npm install
